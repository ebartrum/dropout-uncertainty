Experiment with sampling dropout to estimate uncertainty of neural networks
using Keras, as described in [this blog post by Yarin Gal]
(http://mlg.eng.cam.ac.uk/yarin/blog_3d801aa532c1ce.html).